var canvas_width, canvas_height

var Neuron = synaptic.Neuron,
    Layer = synaptic.Layer,
    Network = synaptic.Network,
    Trainer = synaptic.Trainer,
    Architect = synaptic.Architect;

var inputLayer = new Layer(2);
var hiddenLayer = new Layer(10);
var outputLayer = new Layer(1);

inputLayer.project(hiddenLayer);
hiddenLayer.project(outputLayer);

var myNetwork = new Network({
    input: inputLayer,
    hidden: [hiddenLayer],
    output: outputLayer
});

var learning_rate = 0.2
var X = []
var Y = []

var rows, cols, resolution
var type = 1

function setup () {

    canvas_width = windowWidth
    canvas_height = windowHeight

    resolution = 20
    rows = canvas_height / resolution
    cols = canvas_width / resolution

    createCanvas(canvas_width, canvas_height)
    setInterval(train, 1)
}

function draw () {
    background(51)
    frameRate(30)

    // train()

    plot()
    plotData()
    intro()
}

function train () {
    for (let i = 0; i < Y.length; i++) {
        myNetwork.activate(X[i])
        myNetwork.propagate(learning_rate, Y[i])
    }
    console.log("Czekaj")
}

function predict () {
    for (let i = 0; i < Y.length; i++) {
        console.log(myNetwork.activate(X[i]));
    }
}

function plot () {
    for (let i = 0; i < cols; i++) {
        for (let j = 0; j < rows; j++) {
            noStroke();
            let y = myNetwork.activate([i / cols, j / rows]);
            fill(y * 255);
            rect(i * resolution, j * resolution, resolution, resolution);
        }
    }
}

function mouseClicked () {
    if (mouseX < width && mouseY < height) {
        let normX1 = map(mouseX, 0, width, 0, 1)
        let normX2 = map(mouseY, 0, height, 0, 1)

        X.push([normX1, normX2])
        Y.push([type]);
    }
}

function swap() {
    type = 1 - type;
}

function keyPressed () {
    if (keyCode === SHIFT) {
        swap()
    }
}

function plotData () {
    noStroke();
    for (var i = 0; i < Y.length; i++) {
        Y[i] == 1 ? fill(255, 0, 0) : fill(0, 0, 255);
        let denormX = Math.floor(map(X[i][0], 0, 1, 0, width))
        let denormY = Math.floor(map(X[i][1], 0, 1, 0, height))
        stroke(255);
        ellipse(denormX, denormY, 8);
    }
    noFill();
}

function intro () {
    fill(100)
    noStroke()
    textFont('monospace')
    textSize(20)

    let instruction = "Kliknij na polu aby wstawic punkt...                                Nacisnij SHIFT aby zmienic kolor"
    text(instruction, 15, windowHeight - 30)

    noFill();
    noStroke();
}